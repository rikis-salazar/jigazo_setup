#!/usr/bin/sh
#
# This script assumes that
# 1.  the .air file has been unzipped at /opt/airapps/jigazo; and
# 2.  the Adobe AIR TM binaries are installed at /opt/adobe-air-sdk
#
/opt/adobe-air-sdk/bin/adl -nodebug /opt/airapps/jigazo/META-INF/AIR/application.xml /opt/airapps/jigazo/
