# Install Ji Ga Zo (linux platform)

> Requirements for linux (according to the box)
>
> -   Processor:        2GHz
> -   Memory:           1Gb
> -   OS:               Fedora 8, Ubuntu 7.10, OpenSuse 10.3 (theory)
>                       ArchLinux (practice)
> -   Printer:          600 dpi B&W
> -   CD-ROM:           4x


### Installation (theory)

Plug in CD, then follow instructions (Yeah, right!)


### Installation (practice)

The app uses Adobe Air TM. One needs to use version 2.6 or earlier on linux
because they stopped supporting linux afterwards.

Note:

*   In order to fulfill all 32bit dependencies, the _multilib_ repository must
    be enabled.

Steps:

*   Install Adobe AIR (e.g., in Arch Linux `yay adobe-air-sdk`).
*   Follow the instructions [here][arch-instructions].

Note:

Files with extension `.air` are just zip files.


[arch-instructions]: https://wiki.archlinux.org/index.php/Adobe_AIR
